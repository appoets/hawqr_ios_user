//
//  ServiceTypeCell.swift
//  GoJekUser
//
//  Created by Ansar on 26/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit

class CourierServiceTypeCell: UICollectionViewCell {
    
    @IBOutlet weak var serviceImage:UIImageView!
    @IBOutlet weak var serviceNameLbl: UILabel!
    @IBOutlet weak var estimationtimeLbl: UILabel!
    @IBOutlet weak var estimatBaseFare: UILabel!
    @IBOutlet weak var inFoButton: UIButton!
    
    
    
    var isCurrentService = false
    {
        didSet {
//            estimationtimeLbl.textColor = isCurrentService ? .fliterColor : .darkGray
            serviceNameLbl.textColor = isCurrentService ? .black : .black
            serviceNameLbl.backgroundColor = isCurrentService ? .appPrimaryColor : .clear
//            serviceNameLbl.backgroundColor = .clear
            serviceNameLbl.layer.cornerRadius = serviceNameLbl.frame.height/2
            estimatBaseFare.textColor = isCurrentService ? .black : .black
//            estimatBaseFare.backgroundColor = isCurrentService ? .fliterColor : .boxColor
            
        }
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        initialLoads()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        self.serviceImage.makeRounded()
        serviceNameLbl.layer.cornerRadius = serviceNameLbl.frame.height/2
        serviceNameLbl.clipsToBounds = true
        
        estimatBaseFare.layer.cornerRadius = 4
        estimatBaseFare.clipsToBounds = true
    }
    
    
    @IBAction func InFoButtonAction(_ sender: Any) {
    }
    
    
}


//MARK: - Methods

extension CourierServiceTypeCell {
    private func initialLoads() {
        self.serviceImage.contentMode = .scaleAspectFill
        serviceNameLbl.font = UIFont.setCustomFont(name: .medium, size: .x12)
        estimatBaseFare.font = UIFont.setCustomFont(name: .medium, size: .x16)
        
        estimationtimeLbl.font = .setCustomFont(name: .medium, size: .x14)

        serviceNameLbl.layer.cornerRadius = serviceNameLbl.frame.height/2
        estimatBaseFare.layer.cornerRadius = 4
        setDarkMode()
    }
    
    func setDarkMode(){
        self.backgroundColor = .whiteColor
        self.serviceNameLbl.backgroundColor = .whiteColor
        self.estimatBaseFare.backgroundColor = .clear
    }
    
    
}
