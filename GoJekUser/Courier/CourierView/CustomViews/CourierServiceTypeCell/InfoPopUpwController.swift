//
//  InfoPopUpwController.swift
//  GoJekUser
//
//  Created by Bhuvi on 12/04/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class InfoPopUpwController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var mainView: UIView!
    
    
    @IBOutlet weak var titleLable: UILabel!
//    @IBOutlet weak var contentLable: UILabel!
    
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var okButton: UIButton!
    
    
    var vehicleContent = String()
    var vehicleName = String()
    var vehicleServices : [Services]?
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLable.text = vehicleName
        self.contentTextView.text = vehicleContent
      
    }
    

    @IBAction func okButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)

        
    }
    

}
