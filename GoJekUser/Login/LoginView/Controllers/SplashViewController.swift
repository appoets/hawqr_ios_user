//
//  SplashViewController.swift
//  GoJekUser
//
//  Created by Rajes on 06/04/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
import GooglePlaces
import Stripe
import NVActivityIndicatorView
import AVFoundation
import AVKit

class SplashViewController:  UIViewController,AVPlayerViewControllerDelegate {
    
    @IBOutlet weak var retryButton:UIButton!
    @IBOutlet weak var skipBtn:UIButton!
    @IBOutlet weak var videoLoadView:UIView!
    var activityIndicatorView: NVActivityIndicatorView!
    var playerController = AVPlayerViewController()
    
    var player = AVPlayer()
    
    @IBAction func SkipBtnAction(_ sender: Any) {
        player.pause()
        player.isMuted = true
        player.volume = 0
        playerController.player?.pause()
        if checkAlreadyLogin() {
            UserDefaults.standard.set(1, forKey: "first_install")
            retryButton.isHidden = true

            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            appDelegate.window?.rootViewController = TabBarController().listTabBarController()
            appDelegate.window?.makeKeyAndVisible()
        } else {
            UserDefaults.standard.set(1, forKey: "first_install")
            retryButton.isHidden = false

            let walkThroughViewcontroller =  LoginRouter.loginStoryboard.instantiateViewController(withIdentifier: LoginConstant.WalkThroughController)
            navigationController?.pushViewController(walkThroughViewcontroller, animated: true)
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        var playerController = AVPlayerViewController()
        

       
        loginPresenter?.getBaseURL(param: [LoginConstant.salt_key: APPConstant.salt_key])
        retryButton.titleLabel?.font = .setCustomFont(name: .medium, size: .x16)
        retryButton.addTarget(self, action: #selector(tapRetry), for: .touchUpInside)
        retryButton.isHidden = true
        retryButton.setTitle("Please try again", for: .normal)
        // While launching splash if any internet problem means, once app comes to foreground this api will work
        NotificationCenter.default.addObserver(self, selector: #selector(appComesForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        addGifLoader()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        let path = Bundle.main.path(forResource: "intro_video", ofType: "mp4")
        
        let url = NSURL(fileURLWithPath: path ?? "")
        
        let player = AVPlayer(url:url as URL)

        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        playerLayer.videoGravity = .resizeAspectFill
        self.videoLoadView.layer.addSublayer(playerLayer)
        playerController = AVPlayerViewController()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.didfinishplaying(note:)),name:NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        
        
        
        if (UserDefaults.standard.value(forKey: "first_install") as? Int ?? 0)
            != 1{
            skipBtn.isHidden = false
            playerController.player = player
            
            // playerController.allowsPictureInPicturePlayback = true
            
            playerController.delegate = self
            //            playerController.videoGravity = .resizeAspectFill
            playerController.player?.play()
            
            //            self.present(playerController,animated:true,completion:nil)
            playVideo(from: "intro_video.mp4")
        }else{
            videoLoadView.isHidden = true
            skipBtn.isHidden = true
        }
    }
    
    
    private func playVideo(from file:String) {
        let file = file.components(separatedBy: ".")

        guard let path = Bundle.main.path(forResource: file[0], ofType:file[1]) else {
            debugPrint( "\(file.joined(separator: ".")) not found")
            return
        }
        player = AVPlayer(url: URL(fileURLWithPath: path))

         player.play()
        self.view.bringSubviewToFront(self.skipBtn)
    }
    
     @objc func tapRetry() {
        loginPresenter?.getBaseURL(param: [LoginConstant.salt_key: APPConstant.salt_key])

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func appComesForeground(notification: NSNotification) {
         loginPresenter?.getBaseURL(param: [LoginConstant.salt_key: APPConstant.salt_key])
    }
   
    func addGifLoader(){
       
   activityIndicatorView =  NVActivityIndicatorView(frame:  CGRect(x: self.view.frame.size.width/2 - 50, y:  self.view.frame.height - 80, width: 80, height: 80), type: .ballPulse, color: .white, padding: 20)
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
     }
    func checkAlreadyLogin() -> Bool {
        let fetchData = try! PersistentManager.shared.context.fetch(LoginData.fetchRequest()) as? [LoginData]
        if (fetchData?.count ?? 0) <= 0 {
            return false
        }
        AppManager.shared.accessToken = fetchData?.first?.access_token
        print(fetchData?.first?.access_token ?? "")
        return (fetchData?.count ?? 0) > 0
    }
    func getCountries() {
        let param: Parameters = [LoginConstant.salt_key : APPConstant.salt_key]
       loginPresenter?.getCountries(param: param)
    }
    
    
    @objc func didfinishplaying(note : NSNotification)
         {
             UserDefaults.standard.set(1, forKey: "first_install")
             if checkAlreadyLogin() {
                 retryButton.isHidden = true

                 let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                 appDelegate.window?.rootViewController = TabBarController().listTabBarController()
                 appDelegate.window?.makeKeyAndVisible()
             } else {
                 retryButton.isHidden = false

                 let walkThroughViewcontroller =  LoginRouter.loginStoryboard.instantiateViewController(withIdentifier: LoginConstant.WalkThroughController)
                 navigationController?.pushViewController(walkThroughViewcontroller, animated: true)
             }
             
             

         }
    
    
    func playerViewController(_: AVPlayerViewController, timeToSeekAfterUserNavigatedFrom: CMTime, to: CMTime){
        print("Called when scrubb")
    }
    func playerViewController(_: AVPlayerViewController, willResumePlaybackAfterUserNavigatedFrom: CMTime, to: CMTime){
        print("Called when scrubs")
    }
    
    func playerViewControllerDidStopPictureInPicture(_ playerViewController: AVPlayerViewController) {
        print("Called when scrubs")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func playerViewController(_ playerViewController: AVPlayerViewController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
                 let currentviewController =  navigationController?.visibleViewController

                 if currentviewController != playerViewController
                 {
                   //  currentviewController?.present(playerViewController,animated: true,completion:nil)
                 }
      
    }
        @IBAction func skip(_ sender: Any) {
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                  appDelegate.window?.rootViewController = TabBarController().listTabBarController()
                  appDelegate.window?.makeKeyAndVisible()
    //        let vc = self.storyboard?.instantiateViewController(withIdentifier: LoginConstant.SignInController) as! SignInController
    //        navigationController?.pushViewController(vc, animated: true)
            
            
        }
        
}

extension SplashViewController: LoginPresenterToLoginViewProtocol {
    
    func getBaseURLResponse(baseEntity: BaseEntity) {
        activityIndicatorView.stopAnimating()
        AppConfigurationManager.shared.baseConfigModel = baseEntity
        AppConfigurationManager.shared.setBasicConfig(data: baseEntity)
        getCountries()
        CommonFunction.setDynamicMapStripeKey()
       
        if (UserDefaults.standard.value(forKey: "first_install") as? Int ?? 0)
            == 1{
        if checkAlreadyLogin() {
            retryButton.isHidden = true

            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            appDelegate.window?.rootViewController = TabBarController().listTabBarController()
            appDelegate.window?.makeKeyAndVisible()
        } else {
            retryButton.isHidden = false

            let walkThroughViewcontroller =  LoginRouter.loginStoryboard.instantiateViewController(withIdentifier: LoginConstant.WalkThroughController)
            navigationController?.pushViewController(walkThroughViewcontroller, animated: true)
        }
        }else{
            
        }
        
//           let walkThrough = CourierRouter.courierStoryboard.instantiateViewController(withIdentifier: CourierConstant.DeliveryTypeViewController)
//          navigationController?.pushViewController(walkThrough, animated: true)
    }
    
    
    
    func getCountries(countryEntity: CountryEntity) {
        AppManager.shared.saveCountries(countries: countryEntity.countryData ?? [CountryData]())
    }
    
    func failureResponse(failureData: Data) {
        retryButton.isHidden = false
        let walkThroughViewcontroller =  LoginRouter.loginStoryboard.instantiateViewController(withIdentifier: LoginConstant.WalkThroughController)
        navigationController?.pushViewController(walkThroughViewcontroller, animated: true)
    }
}
