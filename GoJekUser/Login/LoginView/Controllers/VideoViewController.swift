//
//  VideoViewController.swift
//  GoJekUser

//  Created by Elakkiya on 21/02/22.
//  Copyright © 2022 Appoets. All rights reserved.


import UIKit
import AVFoundation
import AVKit


class VideoViewController: UIViewController,AVPlayerViewControllerDelegate {
    var playerController = AVPlayerViewController()
    @IBOutlet weak var ImgView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        var playerController = AVPlayerViewController()
        

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        let path = Bundle.main.path(forResource: "intro_video", ofType: "mp4")

        let url = NSURL(fileURLWithPath: path ?? "")

        let player = AVPlayer(url:url as URL)

        playerController = AVPlayerViewController()


        NotificationCenter.default.addObserver(self, selector: #selector(self.didfinishplaying(note:)),name:NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)

        playerController.player = player

        playerController.allowsPictureInPicturePlayback = true

        playerController.delegate = self

        playerController.player?.play()
        
        self.present(playerController,animated:true,completion:nil)

    }
@IBOutlet weak var play: UIImageView!
    
@objc func didfinishplaying(note : NSNotification)
     {
          let appDelegate = UIApplication.shared.delegate! as! AppDelegate
          appDelegate.window?.rootViewController = TabBarController().listTabBarController()
          appDelegate.window?.makeKeyAndVisible()

   
//
    

//         playerController.dismiss(animated: true,completion: nil)
//         let alertview = UIAlertController(title:"finished",message:"video finished",preferredStyle: .alert)
//         alertview.addAction(UIAlertAction(title:"Ok",style: .default, handler: nil))
//         self.present(alertview,animated:true,completion: nil)
     }
func playerViewController(_ playerViewController: AVPlayerViewController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
             let currentviewController =  navigationController?.visibleViewController

             if currentviewController != playerViewController
             {
                 currentviewController?.present(playerViewController,animated: true,completion:nil)
             }
    
    



/*
// MARK: - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
}
*/

}
    @IBAction func skip(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
              appDelegate.window?.rootViewController = TabBarController().listTabBarController()
              appDelegate.window?.makeKeyAndVisible()
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: LoginConstant.SignInController) as! SignInController
//        navigationController?.pushViewController(vc, animated: true)
        
        
    }
    }
   
   

